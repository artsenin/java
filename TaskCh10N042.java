import java.util.Scanner;
public  class  TaskCh10N042
{
	public static void main(String args[])
	{	
		int x,n;
		int pow;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x,n:");
		x = sc.nextInt();
		n = sc.nextInt();

		if  (n != 0)
		{			
			pow = power(x,n);
		}
		else
			pow = 1;
		System.out.println(x+" pow " + n + " = " + pow);
	}
	public static int power(int x, int n)
	{
		if (n == 1)
		return x;
		else 
		return x * (power(x,n-1));
	}
}
