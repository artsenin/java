public class TaskCh12N025{
	public static void print_array(int[] [] arr)
        {
                for (int x = 0; x < arr.length; x++)
                        {
                                for (int y = 0; y < arr[0].length ; y++)
                                {
                                        System.out.print(arr[x] [y] +" ");
                                }
                        System.out.print("\n");
                        }
        }



	public static void main(String args[])
	{
		int[] [] arr_a = new int[12] [10];
		int n = 1;
		{
			for (int x = 0 ; x < 12; x++)
			{
				for (int y = 0 ; y < 10; y++)
					{
					arr_a[x] [y] = n++;
					}
			}
		}


	
		int[] [] arr_b = new int[12] [10];
		n = 1;
		{
			for (int y = 0 ; y < 10; y++)
			{
				for (int x = 0 ; x < 12; x++)
					{
					arr_b[x] [y] = n++;
					}
			}
		}

		int[] [] arr_v = new int[12] [10];
		n = 1;
		{
			for (int x = 0 ; x < 12; x++)
			{
				for (int y = 9 ; y >= 0; y--)
					{
					arr_v[x] [y] = n++;
					}
			}
		}

		int[] [] arr_g = new int[12] [10];
		n = 1;
		{
			for (int y = 0 ; y < 10; y++)
			{
				for (int x = 11; x >= 0; x--)
					{
					arr_g[x] [y] = n++;
					}
			}
		}

		int[] [] arr_d = new int[10] [12];
		n = 1;
		{
			for (int x = 0 ; x < 10; x++)
			{
				if ((x%2) == 0)
				{	
					for (int y = 0; y < 12; y++)
						{
						arr_d[x] [y] = n++;
						}
				}
				else 
				{
					for (int y = 11; y >= 0; y--)
						{
						arr_d[x] [y] = n++;
						}
				}
			}
		}
	
		int[] [] arr_e = new int[12] [10];
		n = 1;
		{
			for (int y = 0 ; y < 10; y++)
			{
				if ((y%2) == 0)
				{	
					for (int x = 0; x < 12; x++)
						{
						arr_e[x] [y] = n++;
						}
				}
				else 
				{
					for (int x = 11; x >= 0; x--)
						{
						arr_e[x] [y] = n++;
						}
				}
			}
		}
	
		int[] [] arr_zh = new int[12] [10];
		n = 1;
		{
			for (int x = 11 ; x >= 0; x--)
			{
				for (int y = 0 ; y < 10; y++)
					{
					arr_zh[x] [y] = n++;
					}
			}
		}

		int[] [] arr_z = new int[12] [10];
		n = 1;
		{
			for (int y = 9 ; y >= 0; y--)
			{
				for (int x = 0 ; x < 12; x++)
					{
					arr_z[x] [y] = n++;
					}
			}
		}

		int[] [] arr_i = new int[12] [10];
		n = 1;
		{
			for (int x = 11 ; x >= 0; x--)
			{
				for (int y = 9 ; y >= 0; y--)
					{
					arr_i[x] [y] = n++;
					}
			}
		}

		int[] [] arr_k = new int[12] [10];
		n = 1;
		{
			for (int y = 9 ; y >= 0; y--)
			{
				for (int x = 11; x >= 0; x--)
					{
					arr_k[x] [y] = n++;
					}
			}
		}

		int[] [] arr_l = new int[12] [10];
		n = 1;
		{
			for (int x = 11 ; x >= 0; x--)
			{
				if ((x%2) == 1)
				{	
					for (int y = 0; y < 10; y++)
						{
						arr_l[x] [y] = n++;
						}
				}
				else 
				{
					for (int y = 9; y >= 0; y--)
						{
						arr_l[x] [y] = n++;
						}
				}
			}
		}

		int[] [] arr_m = new int[12] [10];
		n = 1;
		{
			for (int x = 0 ; x < 12; x++)
			{
				if ((x%2) == 1)
				{	
					for (int y = 0; y < 10; y++)
						{
						arr_m[x] [y] = n++;
						}
				}
				else 
				{
					for (int y = 9; y >= 0; y--)
						{
						arr_m[x] [y] = n++;
						}
				}
			}
		}

		int[] [] arr_n = new int[12] [10];
		n = 1;
		{
			for (int y = 9 ; y >= 0; y--)
			{
				if ((y%2) == 1)
				{	
					for (int x = 0; x < 12; x++)
						{
						arr_n[x] [y] = n++;
						}
				}
				else 
				{
					for (int x = 11; x >= 0; x--)
						{
						arr_n[x] [y] = n++;
						}
				}
			}
		}

		int[] [] arr_o = new int[12] [10];
		n = 1;
		{
			for (int y = 0 ; y < 10; y++)
			{
				if ((y%2) == 1)
				{	
					for (int x = 0; x < 12; x++)
						{
						arr_o[x] [y] = n++;
						}
				}
				else 
				{
					for (int x = 11; x >= 0; x--)
						{
						arr_o[x] [y] = n++;
						}
				}
			}
		}

		int[] [] arr_p = new int[12] [10];
		n = 1;
		{
			for (int x = 11 ; x >= 0; x--)
			{
				if ((x%2) == 0)
				{	
					for (int y = 0; y < 10; y++)
						{
						arr_p[x] [y] = n++;
						}
				}
				else 
				{
					for (int y = 9; y >= 0; y--)
						{
						arr_p[x] [y] = n++;
						}
				}
			}
		}

		int[] [] arr_r = new int[12] [10];
		n = 1;
		{
			for (int y = 9 ; y >= 0; y--)
			{
				if ((y%2) == 0)
				{	
					for (int x = 0; x < 12; x++)
						{
						arr_r[x] [y] = n++;
						}
				}
				else 
				{
					for (int x = 11; x >= 0; x--)
						{
						arr_r[x] [y] = n++;
						}
				}
			}
		}



	print_array(arr_r);
	}
}
