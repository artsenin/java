import java.io.*;
public class TaskCh03N026 {
	public static void main(String args[]){
	char example_number = 'a';
	write_result(example_number);
	example_number = 'b';
	write_result(example_number);
	example_number = 'c';
	write_result(example_number);
	}

//а) не (X или Y) и (не X или не Z);
//б) не (не X и Y) или (X и не Z);
//в) X или не Y и не (X или не Z).
	
	public static void write_result(char example_number)
	{	
		boolean x,y,z,sub_expr1,sub_expr2,res;
		int a, b, c;	
		try(FileWriter result = new FileWriter("./result",true))
		{
			result.write("Ex " + example_number + "\n");		
			result.write("xyz"+"\t"+" sub_expr1 "+" sub_expr "+"  res"+"\n");
			for (a = 0 ; a < 2 ; a++)
			{
				for (b = 0 ; b < 2 ; b++)
				{
					for (c = 0 ; c < 2 ; c++)
					{
					if (a == 0) x = false;
					else x = true;
					if (b == 0) y = false;
					else y = true;
					if (c == 0) z = false;
					else z = true;
								if (example_number == 'a')
					{
						
						sub_expr1 = !(x|y);
						sub_expr2 = (!x|!z);
						res = sub_expr1&sub_expr2;
					}
					else if (example_number == 'b')
					{
						sub_expr1 = !x&y;
						sub_expr2 = x&!z;
						res = !sub_expr1|sub_expr2;
					}
					else 
					{
						sub_expr1 = x;
						sub_expr2 = !y&!(x|!z);
						res = sub_expr1|sub_expr2;
					}
					result.write(String.valueOf(a));
					result.write(String.valueOf(b));
					result.write(String.valueOf(c));
					result.append('\t');
					result.append('\t');
					if (sub_expr1)	result.write(String.valueOf(1));
					else 	result.write(String.valueOf(0));
 					result.append('\t');
					if (sub_expr2)	result.write(String.valueOf(1));
					else 	result.write(String.valueOf(0));
					result.append('\t');
					if (res) result.write(String.valueOf(1));
					else 	result.write(String.valueOf(0));
					result.append('\n');
					}
				}
			}
		result.append('\n');
		}
		catch(IOException e){
                System.out.println(e.getMessage());
       		}	 
	}
}
