import java.util.Scanner;
public  class  TaskCh10N041
{
	public static void main(String args[])
	{	
		int number;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter n:");
		number = sc.nextInt();
		if  (number != 0)
		{			
			System.out.println(fact(number));
		}
		else
			System.out.println("Number cannot be 0");
	}
	public static int fact(int n)
	{
		if (n == 1)
		return 1;
		else 
		return n * fact(n-1);
	}
}
