import java.util.Scanner;
public class TaskCh12N024{
	public static int[] [] arr_a = new int[6] [6];
	public static void main(String args[])
	{
			Scanner sc = new Scanner(System.in);
		System.out.println("Enter n");
		int n = sc.nextInt();
		int[] [] arr_b = new int[n] [n];

		for (int i = 0 ; i < 6  ; i++)
		{
			for (int j = 0 ; j < 6 ; j++)
			{
				if ((i == 0)|(j == 0))
				{
					arr_a[i] [j] = 1;
				}	
				else
				{
					arr_a[i] [j] = task_a(i-1,j);
				}
			}
		}

		for (int i = 0 ; i < n  ; i++)
		{
			for (int j = 0 ; j < n ; j++)
			{
				arr_b[i] [j] = task_b(i,j,n);
			}
		}



	System.out.println("Ex.a");
	print_array(arr_a);
	System.out.println("Ex.b");
	print_array(arr_b);

	}

	public static int task_a(int i,int j)	
	{		
		if (j == 0)
		return  arr_a[i] [j] ; 
		else 
		return arr_a[i] [j] + task_a(i,j-1);	
	}


	public static int task_b(int i,int j,int n)	
	{	
			
		return   (i+j) % n + 1;
	}


	public static void print_array(int[] [] arr)
	{
		for (int i = 0; i < arr.length; i++)
			{
				for (int j = 0; j < arr.length; j++)
				{
					System.out.print(arr[i] [j] +" ");
				}
			System.out.print("\n");
			}
	}

	
}
