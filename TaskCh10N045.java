import java.util.Scanner;
public  class  TaskCh10N045
{
	public static void main(String args[])
	{	
		int n;
		int x1,xn,delta,summa;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x1:");
		x1 = sc.nextInt();
		System.out.println("Enter n:");
		n = sc.nextInt();
		System.out.println("Enter delta:");
		delta = sc.nextInt();

		if  (n < 1) 
			System.out.println(" n can't be zero" );

		else
			xn = get_xn(x1,n,delta);
		System.out.println( n + "-th element is " +  get_xn(x1,n,delta));
		xn = x1+(n-1)*delta;
		summa = get_sum(xn,n,delta);
		System.out.println( "Sum of " + n + " elements is " +  summa);

	}
	public static int get_xn(int x1,int n, int delta)
	{
		if (n < 2)
		return x1;
		else 
		return get_xn(x1,n-1,delta) + delta;
	}
	
	public static int get_sum(int xn,int n,int delta)
	{
		if (n < 2)
		return xn;
		else 
		return  xn + get_sum(xn - delta,n-1,delta);
 	}
}
