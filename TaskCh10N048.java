import java.util.Scanner;
public  class  TaskCh10N048
{
	public static void main(String args[])
	{	
		int[] arr = {1,2,9,8,5,6,7,10};
		int max_value = get_max(arr,arr.length-1);
		System.out.println(max_value);

	}
	public static int get_max(int arr[],int n)
	{
		if (arr[n]>arr[n-1])
			return arr[n];
		else 
			return get_max(arr,n-1);
	}	
}

