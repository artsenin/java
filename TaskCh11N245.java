public class TaskCh11N245{
	public static void main(String args[])
	{
		int[] SrcArr = {1,2,-5,-7,3,5,3,-3,4,-6};
		int[] DstArr = new int[SrcArr.length];
		int cur_positive_position = 0;
		int cur_negative_position = DstArr.length-1;

		for (int i = 0;i<SrcArr.length;i++)
		{
			if (SrcArr[i] >= 0)
			{
				DstArr[cur_positive_position] = SrcArr[i];
				cur_positive_position++;
			}
			else 
			{
				DstArr[cur_negative_position] = SrcArr[i];
				cur_negative_position--;
			}
		}
	reverse(DstArr,cur_negative_position+1,DstArr.length-1);
	for (int i = 0;i<DstArr.length;i++)
	{
		System.out.println(DstArr[i]);
	}	


	}


	public static void reverse(int[] arr, int low, int high) 
	{
                if (high < low)
                        return;
                int temp = arr[low];
                arr[low] = arr[high];
                arr[high ] = temp;
                reverse(arr, low + 1, high - 1);
        }
}
