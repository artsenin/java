import java.io.*;
public  class  TaskCh10N051
{
	public static void main(String args[])
	{
		int x = 5 ;

		getInt_a(x);
		getInt_b(x);
		getInt_c(x);

	}


 public static void  getInt(int n)
        {
                if (n > 0 )
                {
                        try(FileWriter result = new FileWriter("./TaskCh10N051.ActualOutputs.txt",false))
                        {
                                result.write(n);
                                getInt(n-1);
                        }
                        catch (IOException e){
                        System.out.println(e.getMessage());
                        }


                }
        }


    
	public static void  getInt_a(int n)
	{       
		if (n > 0 )
		{
			try(FileWriter result_a = new FileWriter("./TaskCh10N051.ActualOutputs_a.txt",true))
			{
			        result_a.write(n + "");
				getInt_a(n-1);
			}
                        catch (IOException e){
                        System.out.println(e.getMessage());
			}
                }
	}

	public static void  getInt_b(int n)
	{       
		if (n > 0 )
		{
			try(FileWriter result_b = new FileWriter("./TaskCh10N051.ActualOutputs_b.txt",true))
			{
				getInt_b(n-1);
        	                result_b.write(n + "");
			}
			catch (IOException e){
                        System.out.println(e.getMessage());
                        }
                }
	}

	public static void  getInt_c(int n)
	{     
 		if (n > 0 )
		{
			try (FileWriter result_c = new FileWriter("./TaskCh10N051.ActualOutputs_c.txt",true))
			{
				result_c.write(n + "");
				getInt_c(n-1);
			}
			catch (IOException e){
                        System.out.println(e.getMessage());
			}
		}

		try (FileWriter result_c = new FileWriter("./TaskCh10N051.ActualOutputs_c.txt",true))
		{
			result_c.write(n + "");
		}
		catch (IOException e){
                System.out.println(e.getMessage());
		}
	}
}









