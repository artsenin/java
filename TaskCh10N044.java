import java.util.Scanner;
public  class  TaskCh10N044
{
	public static void main(String args[])
	{
		int x, dig_root;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x");
		x = sc.nextInt();
		dig_root = digit_root(x);
		System.out.println(" digit root of " + x + " = " + dig_root);
	}

	public static int digit_root(int x)
	{
		if ((x / 10) == 0)
		return x;
		else 
		return digit_root((x % 10) + digit_root(x / 10));
	}

}
