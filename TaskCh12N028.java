import java.util.*;
public class TaskCh12N028 
{
        public static void main(String args[])
        {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter arr size : ");
		int x = sc.nextInt();
		int arr[][] = new int[x][x];
		int value=1, x_first=0, x_last=x-1, y_first=0, y_last=x-1;

		while(value<=x*x)
                {
                	for(int i=x_first;i<=x_last;i++)
			{
				arr[y_first][i]=value++;
			}

			for(int j=y_first+1;j<=y_last;j++)
			{
				arr[j][x_last]=value++;
			}

			for(int i=x_last-1;i>=x_first;i--)
			{
				arr[y_last][i]=value++;
			}

			for(int j=y_last-1;j>=y_first+1;j--)
			{
				arr[j][x_first]=value++;
			}
			x_first++;
			x_last--;
			y_first++;
			y_last--;
		}
		for(int i=0;i<x;i++)
		{
			for(int j=0;j<x;j++)
			{
				System.out.print(arr[i][j]+ "\t");
			}
			System.out.println();
		}
	}
}

