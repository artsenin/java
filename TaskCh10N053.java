public class TaskCh10N053{
	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5 };
		reverse(arr, 0 , arr.length - 1);
		for (int i = 0; i <arr.length;i++)
			{
				System.out.print(arr[i]);
			}
	System.out.print("\n");

	}

	public static void reverse(int[] arr, int low, int high) {

		if (high < low)
			return;
		int temp = arr[low];
		arr[low] = arr[high];
		arr[high ] = temp;
		reverse(arr, low + 1, high - 1);
	}
}


