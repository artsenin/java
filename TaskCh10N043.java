import java.util.Scanner;
public  class  TaskCh10N043
{
	public static void main(String args[])
	{
		int x, dig_count,dig_sum;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x");
		x = sc.nextInt();
		dig_count = digit_count(x);
		System.out.println(" digit count in " + x + " = " + dig_count);
		dig_sum = digit_sum(x);
		System.out.println(" digit sum in " + x + " = " + dig_sum);
	}

	public static int digit_sum(int x)
	{
		if ((x / 10) == 0)
		return x;
		else 
		return (x % 10) + digit_sum(x / 10);
	}

	public static int digit_count(int x )
		{
			if ((x / 10) == 0)
			return 1;
			else
			return 1 + digit_count(x / 10);
		}
}
