import java.util.Scanner;
public  class  TaskCh10N047
{
	public static void main(String args[])
	{	
		int k;
		int fibo;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter k:");
		k = sc.nextInt();

		if  (k < 0) 
			System.out.println(" k can't be negative" );

		else
			fibo = fib(k);
		System.out.println(" k element is " + fib(k) );
	}
	public static int fib(int k)
	{
		if (k < 3)
		return 1;
		else 
		return fib(k-2)+fib(k-1);
	}
}
