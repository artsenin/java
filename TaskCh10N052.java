//52. Написать рекурсивную процедуру для вывода на экран цифр натурального
//числа в обратном порядке
import java.util.Scanner;

public  class  TaskCh10N052
{
        public static void main(String args[])
        {
                int n;
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter n:");
                n = sc.nextInt();
		System.out.println(reverse(n));
	}
		public static int reverse(int n)
		{
			if (digit_count(n) == 1)
			return n;
			else
			{
			return (n%10)*power(10,digit_count(n)-1) + reverse(n/10);	
			}
		}

		public static int digit_count(int n )
                {
                        if ((n / 10) == 0)
                        return 1;
                        else
                        return 1 + digit_count(n / 10);
                }

		 public static int power(int x, int n)
	        {
        	        if (n == 1)
                	return x;
	                else
        	        return x * (power(x,n-1));
	        }


}
