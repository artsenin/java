import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

class Team{
	String name;
	int[] players;	
}

class Goal{
	int player;
	String time;
	int team;
	int score; 
}



class Game{
	ArrayList<Team> teams;
	ArrayList<Goal> goals;
	Scanner sc = new Scanner(System.in);
	Game(){
		teams = new ArrayList<Team>();
		goals = new ArrayList<Goal>();
		Team team1 = new Team();
		Team team2 = new Team();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter team #1:");
		team1.name = sc.nextLine(); 
		System.out.println("Enter team #2:");
		team2.name = sc.nextLine();
		team1.players = new int[]{13,10,4,3,6,11,32,22,17,25,6};
		team2.players = new int[]{18,43,14,31,62,10,3,11,27,5,63};
		teams.add(team1);
		teams.add(team2);
	
	}

	void play()
		{
			Matcher matcher; 
			Pattern p ;

			System.out.println("Type \"Goal\",team number, player number(0-10) and score separated by tab symbol");
			System.out.println("Type \"Game over\" fot game finish");
			while (sc.hasNextLine()) 
			{
				String line  = sc.nextLine();

	               		p = Pattern.compile("Goal");
		                matcher = p.matcher(line);
		        	if (matcher.find())
               			{
					goal(line);	
				}

				p = Pattern.compile("Game over");
		                matcher = p.matcher(line);
				if (matcher.find())
                		{
					System.out.println(result());
					break;

				}
			}
		}

	void goal(String line)
		{
			String[] record = line.split("\t");
			Goal goal = new Goal();
			goal.player = teams.get(Integer.parseInt(record[1])-1).players[Integer.parseInt(record[2])];
			goal.team = Integer.parseInt(record[1])-1;
			goal.score =  Integer.parseInt(record[3]);
			goals.add(goal);
			System.out.println("Goal!!!!! " + teams.get(Integer.parseInt(record[1])-1).name + " " + "Player #" + goal.player + " " + record[3] +  " scores!");
			System.out.println(this.score());

		}


	String score()
		{
			int score1 = 0;
			int score2 = 0;
			for (int i = 0 ; i < goals.size();i++)
				{
					if (goals.get(i).team == 0)
						score1 = score1 + goals.get(i).score;
					else 	score2 = score2 + goals.get(i).score;

				}
			return ("Score is " + score1 + ":" + score2);
		}

	String result()
		{
			int score1 = 0;
			int score2 = 0;
			String who_won = "";
			for (int i = 0 ; i < goals.size();i++)
				{
					if (goals.get(i).team == 0)
						score1 = score1 + goals.get(i).score;
					else 	score2 = score2 + goals.get(i).score;

				}
			if (score1 > score2) 
				who_won = this.teams.get(0).name  + " won!";
			else if (score2 > score1)
				{
					who_won = this.teams.get(1).name + " won!";
				}
			else if (score2 == score1)
				{
					who_won = "Draw!";
				}
			return (who_won + " Score is " + score1 + ":" + score2);
		}

}
public class TaskCh06N087
{
	public  static void main(String args[])
	{
		String line;
		Game game  = new Game();
		game.play();		
	}
} 
