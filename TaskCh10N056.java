import java.util.Scanner;
import java.util.*;
import java.util.ArrayList;
public  class  TaskCh10N056
{
	public static int step = 1;
	public static ArrayList<Integer> numbers = new ArrayList<Integer>();
	public static void main(String args[])
	{
		int x;
	        Scanner sc = new Scanner(System.in);
		System.out.println("Enter x > 3");
       		x = sc.nextInt();
		try {		
			for (int i = 2 ; i < (x/2 + 1) ; i++)
				{
					numbers.add(i);
				}
			if (isPrime(numbers,x))
				System.out.println(x + " is prime");
			else	System.out.println(x + " is not prime");
		}
		catch (IndexOutOfBoundsException e )
		{
			System.out.println("X must be greater than 3");
		}
	}
    
	public static boolean isPrime(ArrayList<Integer> numbers,int x)
	{
		int n = 1;
	        int first_element;
		if (x%numbers.get(0) == 0)
		{
			return false;
		}
		int max_value = numbers.get(numbers.size()-1);
		first_element = numbers.get(0);
		while ((first_element * n) <= max_value)
		{
			numbers.remove(new Integer(first_element * n));
			n++;
		}
		if (numbers.size() == 0)
		{
			return true;
		}

		else return  isPrime(numbers,x);
	}
}








