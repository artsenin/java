import java.util.Scanner;
public  class  TaskCh10N049
{
	public static void main(String args[])
	{	
		int[] arr = {1,2,9,8,5,6,7,10};
		int max_value = get_max(arr,arr.length-1);
		System.out.println(get_max_position(arr,max_value,0));
	}

    public static int get_max(int arr[],int n)
        {
                if (arr[n]>arr[n-1])
                        return arr[n];
                else
                        return get_max(arr,n-1);
        }


	public static int get_max_position (int arr[],int max_value,int start)
	{
		 if (max_value == arr[start])
	            return start;
	        return get_max_position(arr, max_value,start + 1);
	}

}

