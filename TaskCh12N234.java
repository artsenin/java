public class TaskCh12N234
{

	public static int[] [] arr = {
		   {35,20,28,30},
		   {27,20,24,31},
		   {28,25,33,32},
		   {33,24,22,23},
		   {29,32,20,20},
		   {23,27,21,23},
		   {29,23,28,23},
		   {23,21,27,24},
		   {19,29,36,20},
		   {25,27,25,21},
		   {26,29,23,22}
		};

	public static void main(String args[])
	{
		delete_row(3);
		print_array(arr);
		delete_column(2);
		print_array(arr);
	}

	public static void delete_row(int k)
	{
		for (int i = k ; i< arr.length-1; i++)
		{
			for (int j = 0; j < arr[0].length;j++)
			{
				arr[i] [j] = arr[i+1][j];
				arr[i+1] [j] = 0;
			}
		}
	}



	public static  void delete_column(int s)
	{
		for (int j = s ; j< arr[0].length-1; j++)
		{
			for (int i = 0; i < arr.length;i++)
			{
				arr[i] [j] = arr[i][j+1];
				arr[i] [j+1] = 0;
			}
		}
	}




	public static void print_array(int[] [] arr)
	{
		for (int x = 0; x < arr.length; x++)
                        {
                                for (int y = 0; y < arr[0].length ; y++)
                                {
                                        System.out.print(arr[x] [y] +" ");
                                }
                        System.out.print("\n");
                        }
	        System.out.print("\n");
	}
}
