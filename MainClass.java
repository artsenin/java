abstract class Human{           //This is abstract class. Some methods are abstract and some one - aren't
	String name;
	Human(String name){
		name = this.name;
	} 
	
	void speake(String phrase)
		{
			System.out.println(phrase);
		}
	abstract void doJob();    //  This method is abstract and may be implemented in descendant classes.
}


interface Announcement{
	void tellAnnouncement();
}


interface Introduction{
	void tellIntroduction();
}

interface DescribeSkills{
	void describeSkills();
}

class Employer extends Human implements Announcement {   // Class Employer is descendant of Human and implements Announcement interface.
	Employer(String name){
		super(name);                           //Class Employer overrides the constructor of superclass. 
	}
	final void doJob(){                            //This method cannot be overriden by descendants .
		System.out.println("I do my job");
	}
	
	public void tellAnnouncement(){
		System.out.println("Hi!Introduce yourself and describe your Java experience please!");
	}
}


abstract class Candidat extends Human implements Announcement,Introduction {	

	final void doJob(){
		System.out.println("I'm looking for job");
	}

	Candidat(String name){
		super(name);
		}
	
	public void tellAnnouncement(){
		System.out.println("Hi!");	
	}
	
	public void tellIntroduction(){
		System.out.println("My name is " + this.name);	

	}
}


class GJJ extends Candidat implements DescribeSkills {
	private  int gjj_score;  // The score after GJJ exam
	GJJ(String name){
		super(name);
	}

	void set_gjj_score(int x) {      // We may get/set value to private field foo only through methods set_foo and get_foo. So, gjj_score is encapsulated. 
		gjj_score = x;
	}

	int  get_gjj_score() {     
		return gjj_score ;
	}

	
	public void describeSkills(){  // Implemented interface must be declared as public .
		System.out.println("I passed succesfully getJavaJob exam and code reviews.");
	}
} 

class SelfTeach extends Candidat implements DescribeSkills {

	SelfTeach(String name){
		super(name);
	}

	public void describeSkills(){
                System.out.println("I have been learning Java by myself,nobody examined how through my khowledge and how is my code good");
	}
}

public class MainClass{
	public static void main(String args[])
	{
		int m = 1;
		int n = 1;
		Announcement anno;    //These variables are a interface type. Through this variable we may access only these members of  classes, which implements this interface.
		Introduction intro;   // We will assign to these variables a references to different classes. As result we get a polymorphic behaviour
 		DescribeSkills ds;
		GJJ gjj;
		SelfTeach st;
		Employer empl = new Employer("Employer");
		for (int i = 0 ; i < 10 ; i++)
			{
				if (i % 2 == 0)
				{
					gjj = new GJJ("GJJ-2017-08-" + m); 
 					anno = gjj;
					intro = gjj;
					ds = gjj;
					m++;
					gjj.set_gjj_score(100);
					System.out.println(gjj.get_gjj_score());

				}
				else 
				{
					st = new  SelfTeach("ST-2017-08-" + n);
					anno = st;
					intro = st;
					ds = st;
					n++;
				}

				empl.tellAnnouncement(); //  Polymorphic behaviour : 
				anno.tellAnnouncement(); // The output result depends of class type is refered to by reference variable
				intro.tellIntroduction();
				ds.describeSkills();
			}
	}
}















